# jhlab-config
Configuration files for jhipster lab project.

The following showcases are available:
- jwt
- uaa
- monolith


## 1. Jhipster registry
Functional instance of JHipster registry is the requierment for both jwt and uaa microservice showcases.

Once up and running console of the registry will be available at:
http://localhost:8761/

## 2. JWT Showcase

### 2.1. Generate JWT geodataService 

Generate service skeleton.
```sh
mkdir geodata-service-jwt
cd geodata-service-jwt
jhipster import-jdl ../jhlab-config/jwt/geodata-service-jwt.jdl
jhipster import-jdl ../jhlab-config/geodata-model.jdl
```

### 2.2. Modify generated service code

**Java:**
Change identiy generation mechanism for Country and Currency entities.

From:

```Java
@Id
@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
@SequenceGenerator(name = "sequenceGenerator")
private Long id;
```
To:

```Java
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Long id;
```

**Liquibase:**
Add data sets for coutnries and currencies
Modify entitiy definition files (autoincement i defaultBooleanValue)

```xml
<changeSet id="20201003163503-1" author="jhipster">
    <createTable tableName="country">
        <column name="id" type="bigint" autoIncrement="true">
            <constraints primaryKey="true" nullable="false"/>
        </column>
        ...
        <column name="active" type="boolean" defaultValueBoolean="true">
            <constraints nullable="false" />
        </column>
        <!-- jhipster-needle-liquibase-add-column - JHipster will add columns here -->
    </createTable>
</changeSet>
```

**YAML files:**
application.yml
```yml
eureka:
  client:
    enabled: ${EUREKA_CLIENT_ENABLED:true}
```

application-dev.yml
```yml
datasource:
    type: com.zaxxer.hikari.HikariDataSource
    url: jdbc:postgresql://localhost:5432/jhlab
    username: jwtgeodata
    password: geodatapwd
```
Also remove 'faker' liquibase context so sample data is not loaded automatically.

application-prod.yml


### 2.3. Generate JWT gateway

Generate Gateway skeleton:

```sh
mkdir jhlab-gw-jwt
cd jhlab-gw-jwt
jhipster import-jdl ../jhlab-config/jwt/jhlab-gw.jdl
jhipster import-jdl ../jhlab-config/geodata-model.jdl
```

### 2.4. Modify generated gateway code

application-dev.yml
```yml
datasource:
    type: com.zaxxer.hikari.HikariDataSource
    url: jdbc:postgresql://localhost:5432/jhlab
    username: jwtgateway
    password: gatewaypwd
```
Also remove 'faker' liquibase context so sample data is not loaded automatically.

## 3. UAA Showcase

1. Generate jhlabUaa service

```
jhipster import-jdl jhlab-uaa.jdl
```

2. Generate UAA geodataService 

3. Generate UAA gateway

## 4. Monolith Showcase

