# Postgres setup

## 1. Create database and main role (used by monolith app)

Connect to database with psql client

```
sudo su - postgres
psql
```

Create Database and set password for the default (jhlab) user/role:

```sql
CREATE DATABASE jhlab;
ALTER ROLE jhlab WITH CREATEROLE PASSWORD 'jhlabpwd';
GRANT CREATE ON DATABASE jhlab TO jhlab WITH GRANT OPTION;
```

** Now logout and login to postgres as jhlab user. **

## 2. Create a role for JWT gateway service

```sql
CREATE ROLE jwtgateway with LOGIN ENCRYPTED PASSWORD 'gatewaypwd';
GRANT jwtgateway TO jhlab;
CREATE SCHEMA IF NOT EXISTS AUTHORIZATION jwtgateway;

-- GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO jwt-gateway;
```

## 3. Create a role for JWT geodata service

```sql
CREATE ROLE jwtgeodata with LOGIN ENCRYPTED PASSWORD 'geodatapwd';
GRANT jwtgeodata TO jhlab;
CREATE SCHEMA IF NOT EXISTS AUTHORIZATION jwtgeodata;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO jwtgeodata;
```

## 4. Create a role for UAA gateway service

```sql
CREATE ROLE gatewayUaa with LOGIN ENCRYPTED PASSWORD 'gatewaypwd';
GRANT gatewayUaa TO jhlab;
CREATE SCHEMA IF NOT EXISTS AUTHORIZATION gatewayUaa;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO gatewayUaa;
```

## 5. Create a role for UAA geodata service

```sql
CREATE ROLE geodata-uaa with LOGIN ENCRYPTED PASSWORD 'geodatapwd';
GRANT geodata TO jhlab;
CREATE SCHEMA IF NOT EXISTS AUTHORIZATION geodata;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO geodata;
```
