#!/bin/bash

kubectl create -f k8s/namespace-jhlab.yml

kubectl apply -f k8s/registry/

# shared objects 
kubectl create -f k8s/postgres-host-svc.yml
kubectl create -f k8s/jhlab-config.yml

# ingress services 
kubectl create -f k8s/jhlab-ingress.yml
kubectl create -f k8s/jhlab-gw-jwt/jhlab-gw-ingress.yml

# config and secret maps
kubectl create -f k8s/geodata-service-jwt/geodata-service-config.yml
kubectl create -f k8s/geodata-service-jwt/geodata-service-secret.yml

kubectl create -f k8s/fidata-service-jwt/fidata-service-config.yml
kubectl create -f k8s/fidata-service-jwt/fidata-service-secret.yml

kubectl create -f k8s/jhlab-gw-jwt/jhlab-gw-config.yml
kubectl create -f k8s/jhlab-gw-jwt/jhlab-gw-secret.yml

# Sevices
kubectl create -f k8s/jhlab-gw-jwt/jhlab-gw-service.yml
kubectl create -f k8s/geodata-service-jwt/geodata-service.yml
kubectl create -f k8s/fidata-service-jwt/fidata-service.yml

# Deployments
kubectl create -f k8s/geodata-service-jwt/geodata-service-jwt-postgres.yml
kubectl create -f k8s/fidata-service-jwt/fidata-service-jwt-postgres.yml
kubectl create -f k8s/jhlab-gw-jwt/jhlab-gw-jwt-postgres.yml
