# Kubernetes CoockBook

## JHlab project Landscape

Namespaces:
* jhlab

Config maps:
* jhlab-config - contains common configuration for all jhlab services
* geodata-service-config
* fidata-service-config
* jhlab-gw-config

Secret maps:
* geodata-service-secret
* fidata-service-secret
* jhlab-gw-secret

Services:
* postgres-db-svc - Service of type ExternalName used to access outside database
* geodata-service
* fidata-service
* jhlab-gw-service

Deployments:
* geodata-service-jwt-deployment
* fidata-service-jwt-deployment
* jhlab-gw-jwt-deployment

Ingress:
* jhlab-ingress
* jhlab-gw-ingress
* jhregistry-ingress (optional)

Pods: 
-----
* geodata-service-jwt x1
* fidata-service-jwt x1
* jhlab-gw-jwt x1
* jhipster-registry x2

## Minikube Preparation
To be able to run JHlab configuratio described in this file you need to give minikube more cpu than ganted to him in default cnfiguration.
On macosx run:

```sh
minikube start start --vm=true --cpus=4 --memory 6072
```
Note: vm option is requiered due to the bug in 1.14 version which prevents ingress from running. For more see [https://github.com/kubernetes/minikube/issues/7332](https://github.com/kubernetes/minikube/issues/7332)

On Linux just run:
```sh
minikube start start --cpus=4 --memory 6072
```

Find out IP address of your minikube cluster by running:

```sh
minikube ip
```

This address is referenced in the rest of this documents as MINIKUBE_IP.

### Install Reloader

```sh
kubectl apply -f https://raw.githubusercontent.com/stakater/Reloader/master/deployments/kubernetes/reloader.yaml
```

## Modify local /etc/hosts

To be able to access services/gateway/registry deployed in minikube you need to modiy your local /etc/hosts file and at the end of the file add the two following lines:

```
MINIKUBE_IP     jhlab.ag04.com
MINIKUBE_IP     jhregistry.ag04.com
```
(/etc/hosts)

## Setup access to external Postgres database

### Configure local Postgres

* add the line listen_addresses = '*' to postgres.conf
* add the line host all all 192.168.64.1/24 md5 to pg_hba.conf
* restart the postgres service

First you need to find out the IP address of your minikube bridge.
To do this, ecxecute:

```sh
minikube ssh "route -n | grep ^0.0.0.0 | awk '{ print \$2 }'"
```

In my casse the IP is (192.168.64.1)

Now (if necessary) adjust "externalName" property of the **k8s/postgres-db-svc.yml** by replacing '192.168.64.1' with your value.

```yml
apiVersion: v1
kind: Service
metadata:
  name: postgres-db-svc
  namespace: jhlab
spec: 
  type: ExternalName
  # If necessary adjust ip address to match your environment
  externalName: postgres-host-db.192.168.64.1.nip.io 
  ports:
    - port: 5432
      protocol: TCP
```

## Create Jhlab namespace


## Deploy jhipster-registry

Create all jhispter-registry objects:

```sh
kubectl create -f k8s/registry-k8s/namespace-jhregistry.yml
kubectl create -f k8s/registry-k8s/application-configmap.yml
kubectl create -f k8s/registry-k8s/jhipster-registry.yml
```

Delete all jhipster-registry objects
```
kubectl delete -n jhlab statefulset jhipster-registry
kubectl delete -n jhlab configmap application-config
kubectl delete -n jhlab secret jhregistry-secret
kubectl delete -n jhlab ingresses/jhipster-registry-ingress
```

### Expose jhipster-registry admin console
To access jhipster-registry console from your machine you need to create ingress service

```
kubectl create -f k8s/registry-k8s/jhipster-registry-ingress.yml
```

This service, by default, maps all calls to jhregistry.ag04.com to jhispter-registry service on port 8761.
For this to work on your local machine minikube IP address needs to be mapped to this url in /etc/hosts
```
MINIKUBE_IP     jhregistry.ag04.com
```
Now you can access JHipster registry at https://jhregistry.ag04.com:8761/


## Create services and deployments

```sh
kubectl create -f k8s/jhlab-config.yml
kubectl create -f k8s/postgres-host-svc.yml

kubectl create -f k8s/jhlab-ingress.yml
kubectl create -f k8s/jhlab-gw-jwt/jhlab-gw-ingress.yml

kubectl create -f k8s/geodata-service-jwt/geodata-service-config.yml
kubectl create -f k8s/geodata-service-jwt/geodata-service-secret.yml

kubectl create -f k8s/fidata-service-jwt/fidata-service-config.yml
kubectl create -f k8s/fidata-service-jwt/fidata-service-secret.yml

kubectl create -f k8s/jhlab-gw-jwt/jhlab-gw-config.yml
kubectl create -f k8s/jhlab-gw-jwt/jhlab-gw-secret.yml

kubectl create -f k8s/jhlab-gw-jwt/jhlab-gw-service.yml
kubectl create -f k8s/geodata-service-jwt/geodata-service.yml
kubectl create -f k8s/fidata-service-jwt/fidata-service.yml

kubectl create -f k8s/geodata-service-jwt/geodata-service-jwt-postgres.yml
kubectl create -f k8s/fidata-service-jwt/fidata-service-jwt-postgres.yml
kubectl create -f k8s/jhlab-gw-jwt/jhlab-gw-jwt-postgres.yml
```

## Jhlab-config map 
This map holds common properties that are used by all jhlab objects (gateways, microservices, ...)

```yml
  eureka_client_enabled: "true"
  spring_cloud_config_enabled: "true"
  eureka_client_service_url: "http://admin:${jhipster.registry.password}@jhipster-registry.default.svc.cluster.local:8761/eureka/"
  spring_cloud_config_uri: "http://admin:${jhipster.registry.password}@jhipster-registry.default.svc.cluster.local:8761/config"
```

## Store db credentials in the secure storage

Execute the following command to encode (base64) your secret credentials. For example:

```sh
echo -n 'geodatapwd' | base64
echo -n 'gatewaypwd' | base64
echo -n 'jwtfidatapwd' | base64
```

Once this is done edit:
- kk8s/geodata-service-jwt/geodata-service-secret.yml / and enter correct valuefor the **geodata-db-password** key
- kk8s/fidata-service-jwt/fidata-service-secret.yml / and enter correct valuefor the **fidata-db-password** key
- kk8s/jhlab-jwt/jhlab-jwt-secret.yml / and enter correct valuefor the **jhlabgw-db-password** key

## Redeploy jhlab objects

### Redeploy Deployments
- delete deployment
- create deployment

```sh
kubectl delete deployment.apps/jhlab-gw-jwt-deployment
kubectl delete deployment.apps/geodata-service-jwt-deployment
kubectl delete deployment.apps/fidata-service-jwt-deployment
```

```sh
kubectl delete configmap jhlab-config -n default
```
## Delete all objets

```sh
kubectl delete deployment.apps/jhlab-gw-jwt-deployment -n jhlab
kubectl delete deployment.apps/geodata-service-jwt-deployment -n jhlab
kubectl delete deployment.apps/fidata-service-jwt-deployment -n jhlab

kubectl delete statefulset jhipster-registry -n jhlab
kubectl delete service jhipster-registry -n jhlab 
kubectl delete configmap application-config -n jhlab
kubectl delete secret jhregistry-secret -n jhlab

kubectl delete ingresses ingresses/jhlab-ingress -n jhlab
kubectl delete ingresses/jhlab-gw-ingress -n jhlab

kubectl delete service jhlab-gw-service -n jhlab
kubectl delete service geodata-service -n jhlab
kubectl delete service fidata-service -n jhlab

kubectl delete configmap geodata-service-config -n jhlab
kubectl delete configmap fidata-service-config -n jhlab
kubectl delete configmap jhlab-gw-config -n jhlab
kubectl delete configmap jhlab-config -n jhlab

kubectl delete secret geodata-service-secret -n jhlab
kubectl delete secret fidata-service-secret -n jhlab
kubectl delete secret jhlab-gw-secret -n jhlab

kubectl delete service -f k8s/postgres-host-svc.yml -n jhlab

kubectl delete -n default ingresses/jhregistry-ingress -n jhlab
```
