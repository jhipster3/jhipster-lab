CREATE ROLE jwtgateway with LOGIN ENCRYPTED PASSWORD 'gatewaypwd';
GRANT jwtgateway TO jhlab;
CREATE SCHEMA IF NOT EXISTS AUTHORIZATION jwtgateway;

-- GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO jwt-gateway;

CREATE ROLE jwtgeodata with LOGIN ENCRYPTED PASSWORD 'geodatapwd';
GRANT jwtgeodata TO jhlab;
CREATE SCHEMA IF NOT EXISTS AUTHORIZATION jwtgeodata;

-- GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO jwtgeodata;

CREATE ROLE jwtfidata with LOGIN ENCRYPTED PASSWORD 'jwtfidatapwd';
GRANT jwtfidata TO jhlab;
CREATE SCHEMA IF NOT EXISTS AUTHORIZATION jwtfidata;

GRANT USAGE ON SCHEMA jwtgeodata TO jwtfidata;
GRANT SELECT,REFERENCES ON ALL TABLES IN SCHEMA jwtgeodata TO jwtfidata;
ALTER ROLE jwtfidata SET search_path TO jwtfidata, jwtgeodata;
