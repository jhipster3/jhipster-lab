# Steps to modify the JHipster generated code

## Microservice
1. Add "livorno" profile
- copy: snippets/java/SecurityConfigurationLivorno.java to src/main/java/ .../config

2. Add options to disable Eureka and Cloud config servers
Modify **src/main/resources/config/application.yml** change:

```yml
eureka:
  client:
    enabled: true
```

to
```yml
eureka:
  client:
    enabled: ${EUREKA_CLIENT_ENABLED:true}
```
3. Modify db connection block in application-dev.yml to match this code snippet bellow

```yml
  datasource:
    type: com.zaxxer.hikari.HikariDataSource
    url: jdbc:postgresql://localhost:5432/jhlab
    username: xxxxxxxxxx
    password: yyyyyyyyyy
    hikari:
      poolName: Hikari
      auto-commit: false
  jpa:
    database-platform: io.github.jhipster.domain.util.FixedPostgreSQL10Dialect
    show-sql: true
  liquibase:
    # Remove 'faker' if you do not want the sample data to be loaded automatically
    contexts: dev
```

4. Modify db connection block in application-prod.yml to match this code snippet bellow

```yml
  datasource:
    type: com.zaxxer.hikari.HikariDataSource
    url: ${DATASOURCE_URL}
    username: ${DB_USER}
    password: ${DB_PWD}
    hikari:
      poolName: Hikari
      auto-commit: false
  jpa:
    database-platform: ${JPA_DATABASE_PLATFORM}
    # database-platform: io.github.jhipster.domain.util.FixedPostgreSQL10Dialect
    show-sql: false
  # Replace by 'prod, faker' to add the faker context and have sample data loaded in production
  liquibase:
    contexts: ${LIQUIBASE_CONTEXTS}
    default-schema: ${DB_SCHEMA}
```

5. Change ID generation strategy in all doamin entities

```java
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
```
6. Change ID definition in all liquibase create datasets
```xml
<column name="id" type="bigint" autoIncrement="true">
  <constraints primaryKey="true" nullable="false"/>
</column>
```

6. Mdify gradle docker build
Change gradle/docker.gradle as follows:

```groovy
def imageName = 'ag04/geodata-service-jwt'
def imageVersion = "${version}"
def dockerRegistryUrl = ''

if (project.hasProperty("imageName")) {
    imageName = project.getProperty("imageName");
}

if (project.hasProperty("imageVersion")) {
    imageVersion = project.getProperty("imageVersion")
}

if (project.hasProperty("dockerRegistryUrl")) {
    dockerRegistryUrl = project.getProperty("dockerRegistryUrl") + "/";
}

jib {
    from {
        image = "adoptopenjdk:11-jre-hotspot"
    }
    to {
        image = "${dockerRegistryUrl}${imageName}:${imageVersion}"
    }
```
